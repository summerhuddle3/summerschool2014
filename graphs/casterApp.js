$(function() {

	var d1 = [];
	for (var i = 0; i < 100; i += 1) {
		d1.push([i, Math.sin(i*0.02)]);
	}

	// Start Button
	$('#start').click(function(){
		$('#start').attr('disabled','disabled');
		$('#stop').removeAttr('disabled');
	});


	// Stop Button
	$('#stop').click(function(){
		$('#loadingBar').fadeOut('fast');
		$('#start').removeAttr("disabled");
		$('#stop').attr('disabled','disabled');

		$.getJSON("getCSV.php", function(jsonData){
			//Open the text file(s)
		   	var d2 = jsonData.data;

			$.plot("#placeholder", [{ // Gary
				data: d1,
				lines: { show: true },
				points: { show: false },
				label: "Gary"
			}, { // You
				data: d2,
				lines: { show: true },
				points: { show: false },
				label: "You"
			}]);
		});


		$('#content').fadeIn('fast');

	});


	$('#Again').click(function(){
		$('#content').fadeOut('fast');
		$('#loadingBar').fadeIn('fast');
	});

});