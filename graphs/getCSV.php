<?php

	$file = "./graph.csv";
	$json['label'] = "Your Cast";

	if ($handle = fopen($file, "r")) {
		$contents = fread($handle, filesize($file));

		$plots = explode(';', $contents);

		foreach ($plots AS $p)
		{
			list($time, $speed) = explode(',', trim($p));
			$json['data'][][] = trim($time) . ',' . trim($speed);
		}
	}

	$newData = json_encode($json);
	$newData = str_replace("[\"", "[", $newData);
	$newData = str_replace("\"]", "]", $newData);
	echo $newData;
?>