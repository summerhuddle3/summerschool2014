#include "Calibrate3DSensor.hpp"

Calibrate3DSensor::Calibrate3DSensor(const Calibration & calibration) :
	_calibration(calibration)
{
}

float Calibrate3DSensor::ApplyAxisCalibration(short raw, const AxisCalibration & calibration) const
{
	float result;
	result = raw + calibration.bias;
	result *= calibration.scale;
	return result;
}

Calibrated3DSensorData Calibrate3DSensor::Calibrate(const RawAccelerationData & raw) const
{
	Calibrated3DSensorData result;

	result.x = ApplyAxisCalibration(raw.x, _calibration.x);
	result.y = ApplyAxisCalibration(raw.y, _calibration.y);
	result.z = ApplyAxisCalibration(raw.z, _calibration.z);

	return result;
}
