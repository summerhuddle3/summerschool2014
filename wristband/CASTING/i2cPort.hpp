#ifndef i2c_PORT_H
#define i2c_PORT_H

#include "I2C.hpp"

#include <cstdio>

class i2cPort : public I2C
{
	public:
		i2cPort();
		~i2cPort();
		int i2cData;
		virtual bool SetAddress(unsigned char DeviceAddress);
		virtual bool Write(const void * buffer, int length);
		virtual bool Read(void* buffer, int length);
};

#endif
