	
#pragma once

#include "I2C.hpp"

struct MockI2COperationType
{
	enum value
	{
		SetAddress,
		Read,
		Write
	};
};

struct MockI2COperation
{
	MockI2COperationType::value operation;
	unsigned char address;
	const void * buffer;
	int length;
	bool returnValue;
};

class MockI2C : public I2C
{
private:
	MockI2COperation* _operations;
	int _numberOfOperations;
	int _expectedOperations;

	int _currentOperation;
public:
	MockI2C(MockI2COperation* operations, int numberOfOperations);
	virtual ~MockI2C();
	virtual bool SetAddress(unsigned char address);
	virtual bool Read(void * buffer, int length);
	virtual bool Write(const void * buffer, int length);

	void ExpectSetAddress(unsigned char address, bool returnValue);
	void ExpectRead(const void * buffer, int length, bool returnValue);
	void ExpectWrite(const void * buffer, int length, bool returnValue);

	void Verify();
};

