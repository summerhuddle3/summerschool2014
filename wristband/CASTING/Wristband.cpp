#include "SendSocket.hpp"
#include "DataTransport.hpp"
#include "ConvertedAccelerationData.hpp"
#include "i2cPort.hpp"
#include "I2C.hpp"
#include "BMA150Accelerometer.hpp"

#include <iostream>

//Arguments are Wristband and Phone IP Address

int main(int argc, char* argv[])
{
	const char * ip_address = argv[1];
	int currentId = 0;
	
	i2cPort i2c;
	BMA150Accelerometer accelerometer(i2c);
	SendSocket socket(ip_address); 
	DataTransport gData(&socket);
	
	while(1)
	{
		currentId++;
		RawAccelerationData raw = accelerometer.ReadAcceleration();
		AccelerationDataPayload payload;
		payload.id = currentId;
		payload.data = raw;

		//Calibrate sensor - slimed
		payload.data.x += 3;
		payload.data.y += 20;
		payload.data.z += 120;



		gData.send(&payload);
		std::cout << "Send ID = " << currentId << " :: " 
			<< " X" << payload.data.x
			<< " Y" << payload.data.y
			<< " Z" << payload.data.z
			<< std::endl;
			usleep(10000);
	}

	std::cout << "sentxxx" << std::endl;
	
	return 0;	
}
