#include "hiker.hpp"

short CalculateAccelertion(unsigned char bytes[])
{

    short a = (bytes[1] << 8) + bytes[0];
    return a >> 6;
    

/*
    bytes[0] << 8

    bytes[0]  00110101
    (short)   00000000 00110101

    <<8       00110101 00000000
     bytes[1] 00000000 11101111
         +    00110101 11101111
*/
}