#pragma once

#include "ConvertedAccelerationData.hpp"
#include "Calibrated3DSensorData.hpp"

struct AxisCalibration
{
	short bias;
	float scale;
};

struct Calibration
{
	AxisCalibration x;
	AxisCalibration y;
	AxisCalibration z;
};

class Calibrate3DSensor
{
private:
	Calibration _calibration;

	float ApplyAxisCalibration(short raw, const AxisCalibration & calibration) const;
public:
	Calibrate3DSensor(const Calibration & calibration);
	Calibrated3DSensorData Calibrate(const RawAccelerationData & raw) const;
};
