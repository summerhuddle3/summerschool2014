#ifndef SEND_SOCKET_H
#define SEND_SOCKET_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <cstdlib>
#include <stdio.h>

class SendSocket
{
	public:
		SendSocket(const char* ip_address);
		~SendSocket();
		void SendData(const void*, size_t);
	
	private:
		struct sockaddr_in si_other;
		int sid;
};

#endif
