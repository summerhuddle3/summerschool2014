#include "i2cPort.hpp"

#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

i2cPort::i2cPort()
{
	i2cData = open("/dev/i2c-1", O_RDWR);	
}

i2cPort::~i2cPort()
{
	close(i2cData);
}

bool i2cPort::SetAddress(unsigned char DeviceAddress)
{
	return (ioctl(i2cData, I2C_SLAVE, DeviceAddress) >= 0);
}

bool i2cPort::Write(const void * buffer, int length)
{
	if(length > 0)
		return (write(i2cData, buffer, length) == length);
	return false;
}

bool i2cPort::Read(void* buffer, int length)
{
	if(length > 0)
		return (read(i2cData, buffer, length) == length);
	return false;
}
