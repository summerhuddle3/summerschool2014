#include "DataTransport.hpp"
#include "SendSocket.hpp"
#include "ConvertedAccelerationData.hpp"

DataTransport::DataTransport(SendSocket* s)
:socket(s)
{
	
}

void DataTransport::send(AccelerationDataPayload *payload)
{
	socket->SendData(payload, sizeof(*payload));
}



