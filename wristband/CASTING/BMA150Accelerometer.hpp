#include "I2C.hpp"
#include "Accelerometer.hpp"
#include "i2cPort.hpp"

class BMA150Accelerometer : public Accelerometer
{
private:
	I2C& _i2c;
public:
	explicit BMA150Accelerometer(I2C& i2c);

	virtual RawAccelerationData ReadAcceleration() const;
};

class BMA150AccelerometerCommunicationsError : public AccelerometerException
{
};
