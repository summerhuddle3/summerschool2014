#ifndef CONVERTED_ACCELERATION_DATA_H
#define CONVERTED_ACCELERATION_DATA_H

struct RawAccelerationData
{
	short x;
	short y;
	short z;
};

struct AccelerationDataPayload
{
	RawAccelerationData data;
	int id;	
};

//void conversion(RawAccelerationData*, ConvertedAccelerationData*);

#endif
