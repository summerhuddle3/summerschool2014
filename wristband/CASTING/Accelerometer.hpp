#include <exception>

#include "ConvertedAccelerationData.hpp"

class Accelerometer
{
public:
	virtual ~Accelerometer() { }
	virtual RawAccelerationData ReadAcceleration() const = 0;
};

class AccelerometerException : public std::exception
{
};
