// I2Cdev and BMA150 must be installed as libraries, or else the .cpp/.h files
// for both classes must be in the include path of your project
//#include "I2Cdev.h"
//#include "BMA150.h"

#include "TopRodAccelerometer.hpp"

using namespace std;

void conversion(RawAccelerationData *raw, ConvertedAccelerationData *result)
{
	result->x = (*((short*)(raw->accelX)) >> 6); 
	result->x /= 256; 
	result->y = (*((short*)(raw->accelY)) >> 6); 
	result->y /= 256; 
	result->z = (*((short*)(raw->accelZ)) >> 6); 
	result->z /= 256; 
}
	

