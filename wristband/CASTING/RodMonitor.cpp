#include "RodMonitor.hpp"


RodMonitor::RodMonitor()
{
	//Initialise the variables
	record = false;
	duration = 0;
	start_time = 0;
	stop_time = 0;
}

void RodMonitor::Start()
{
	record = true; //Set record to true
	start_time = time(&t); //Start the timer
}

int RodMonitor::Stop()
{
	//sleep(3);
	int wait;
	std::cin >> wait; 
	record = false; //Set record to false
	stop_time = time(&t); //Stop the timer
	duration = difftime(stop_time, start_time); // calculates the duration
	
	return duration;
}

bool RodMonitor::IsRecording()
{
	return record;
}

/*
void sleep(long wait)
{
	clock_t goal;
	goal = wait + clock();
	while( goal > clock() );
}

*/
