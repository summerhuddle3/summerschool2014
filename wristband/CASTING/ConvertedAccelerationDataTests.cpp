#include "ConvertedAccelerationData.hpp"
#include <gtest/gtest.h>

TEST(conversion, lets_get_a_basic_test_in_place)
{
	RawAccelerationData AccelData;	
	AccelData.accelX[0] = 0x0F; // 93
	AccelData.accelX[1] = 0x4E;
	AccelData.accelY[0] = 0x1F; // 32
	AccelData.accelY[1] = 0x01;
	AccelData.accelZ[0] = 0x2B; // 117
	AccelData.accelZ[1] = 0x4A;

	ConvertedAccelerationData dataset;	
	conversion(&AccelData, &dataset);

	ASSERT_DOUBLE_EQ(1.21875,  dataset.x);
	ASSERT_DOUBLE_EQ(0.015625, dataset.y);
	ASSERT_DOUBLE_EQ(1.15625,  dataset.z);

}

// Step 3. Call RUN_ALL_TESTS() in main().
//
// We do this by linking in src/gtest_main.cc file, which consists of
// a main() function which calls RUN_ALL_TESTS() for us.
//
// This runs all the tests you've defined, prints the result, and
// returns 0 if successful, or 1 otherwise.
//
// Did you notice that we didn't register the tests?  The
// RUN_ALL_TESTS() macro magically knows about all the tests we
// defined.  Isn't this convenient?


	/*
	RawAccelerationData raw;
	raw->accelX = 1;
	raw->accelY = 2;
	raw->accelZ = 3;
	ConvertedAccelerationData cooked;
	conversion(&raw, &cooked);
	ASSERT_NE(1, cooked->x);
	ASSERT_NE(2, cooked->y);
	ASSERT_NE(3, cooked->z);
    */




