#include "BMA150Accelerometer.hpp"

const unsigned char BMA150Address = 0x38;

BMA150Accelerometer::BMA150Accelerometer(I2C& i2c) :
	_i2c(i2c)
{
}

RawAccelerationData BMA150Accelerometer::ReadAcceleration() const
{
	RawAccelerationData result;
	unsigned char registerAddress[] = { 0x02 };

	_i2c.SetAddress(BMA150Address);
	_i2c.Write(registerAddress, sizeof(registerAddress));
	_i2c.Read(&result, sizeof(result));

	result.x >>= 6;
	result.y >>= 6;
	result.z >>= 6;

	return result;
}
