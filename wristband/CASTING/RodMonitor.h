#ifndef ROD_MONITOR_H
#define ROD_MONITOR_H

class RodMonitor
{
public:
   RodMonitor();
   bool IsRecording();
   void Start();
   void Stop();
   int Duration();

private:
	bool record;

};



#endif
