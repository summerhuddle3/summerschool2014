#include <gtest/gtest.h>
#include <memory.h>
#include <iostream>

#include "Mocki2c.hpp"

MockI2C::MockI2C(MockI2COperation * operations, int numberOfOperations) :
	_operations(operations),
	_numberOfOperations(numberOfOperations),
	_expectedOperations(0),
	_currentOperation(0)
{
}

MockI2C::~MockI2C()
{
	Verify();
}

bool MockI2C::SetAddress(unsigned char address)
{
	if(_currentOperation >= _expectedOperations)
	{
		EXPECT_TRUE(_currentOperation < _expectedOperations) << "Unexpected I2C::SetAddress";
		return false;
	}

	if(_operations[_currentOperation].operation != MockI2COperationType::SetAddress)
	{
		EXPECT_EQ(MockI2COperationType::SetAddress, _operations[_currentOperation].operation) << "Unexpected I2C SetAddress operation (" << _currentOperation << ")";
		return false;
	}

	if(_operations[_currentOperation].address != address)
	{
		EXPECT_EQ(address, _operations[_currentOperation].address) << "Incorrect I2C address (" << _currentOperation << ")";
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

bool MockI2C::Read(void * buffer, int length)
{
	if(_currentOperation >= _expectedOperations)
	{
		EXPECT_TRUE(_currentOperation < _expectedOperations) << "Unexpected I2C::Read";
		return false;
	}

	if(_operations[_currentOperation].operation != MockI2COperationType::Read)
	{
		EXPECT_EQ(MockI2COperationType::Read, _operations[_currentOperation].operation) << "Unexpected I2C Read operation (" << _currentOperation << ")";
		return false;
	}

	if(_operations[_currentOperation].length != length)
	{
		EXPECT_EQ(length, _operations[_currentOperation].length) << "Incorrect I2C read data length (" << _currentOperation << ")";
		return false;
	}

	memcpy(buffer, _operations[_currentOperation].buffer, length);
	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

bool MockI2C::Write(const void * buffer, int length)
{
	if(_currentOperation >= _expectedOperations)
	{
		EXPECT_TRUE(_currentOperation < _expectedOperations) << "Unexpected I2C::Write";
		return false;
	}

	if(_operations[_currentOperation].operation != MockI2COperationType::Write)
	{
		EXPECT_EQ(MockI2COperationType::Write, _operations[_currentOperation].operation) << "Unexpected I2C Write operation (" << _currentOperation << ")";
		return false;
	}

	if(_operations[_currentOperation].length != length)
	{
		EXPECT_EQ(length, _operations[_currentOperation].length) << "Incorrect I2C write data length (" << _currentOperation << ")";
		return false;
	}

	if(memcmp(buffer, _operations[_currentOperation].buffer, length))
	{
		EXPECT_EQ(0, memcmp(buffer, _operations[_currentOperation].buffer, length)) << "Incorrect data written to the I2C port (" << _currentOperation << ")";
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;

	return result;
}

void MockI2C::ExpectSetAddress(unsigned char address, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::SetAddress;
	_operations[_expectedOperations].address = address;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	EXPECT_TRUE(_expectedOperations <= _numberOfOperations) << "Attempted to add too many operations to the I2C Mock";
}

void MockI2C::ExpectRead(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::Read;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	EXPECT_TRUE(_expectedOperations <= _numberOfOperations) << "Attempted to add too many operations to the I2C Mock";
}

void MockI2C::ExpectWrite(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::Write;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	EXPECT_TRUE(_expectedOperations <= _numberOfOperations) << "Attempted to add too many operations to the I2C Mock";
}

void MockI2C::Verify()
{
	EXPECT_EQ(_expectedOperations, _currentOperation) << "Not all expected I2C operations have occured";
}
