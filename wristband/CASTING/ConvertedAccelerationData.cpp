#include "ConvertedAccelerationData.hpp"

void conversion(RawAccelerationData * raw, ConvertedAccelerationData * result)
{
	result->x = (*((short*)(raw->accelX)) >> 6); 
	result->x /= 256; 
	result->y = (*((short*)(raw->accelY)) >> 6); 
	result->y /= 256; 
	result->z = (*((short*)(raw->accelZ)) >> 6); 
	result->z /= 256; 
}
