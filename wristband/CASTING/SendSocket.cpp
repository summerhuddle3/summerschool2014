#include "SendSocket.hpp"

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <cstdlib>

#define PORT 2465


void diep(const char *s)
{
	perror(s);
	exit(1);
}
    
SendSocket::SendSocket(const char* ip_address)
{	
	sid = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	
       if (sid == -1)
          diep("socket");
    
       memset((char *) &si_other, 0, sizeof(si_other));
       si_other.sin_family = AF_INET;
       si_other.sin_port = htons(PORT);
       if (inet_aton(ip_address, &si_other.sin_addr) == 0) 
       {
         fprintf(stderr, "inet_aton() failed\n");
         exit(1);
       }      
}

SendSocket::~SendSocket()
{
	close(sid);	
}

void SendSocket::SendData(const void* buf, size_t buf_len)
{
	int slen=sizeof(si_other);

	if (sendto(sid, (char*)buf, buf_len, 0, (const sockaddr*)&si_other, slen) == -1)
	   diep("sendto()");
}
